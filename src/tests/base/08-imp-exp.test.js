import { getHeroeById, getHeroesByOwner } from "../../base-pruebas/08-imp-exp";
import heroes from "../../data/heroes";


describe('Pruebas en funciones de Héroes', () => {

    test('debe de retornar un heroe por id', () => {

        const id = 1;
        const heroe = getHeroeById(id);
        const heroeData = heroes.find(heroe => heroe.id === id);

        expect(heroe).toEqual(heroeData);
    })


    test('debe de retornar undefined si Héroe no existe', () => {

        const id = 10;
        const heroe = getHeroeById(id);

        expect(heroe).toBe(undefined);
    })

    test('debe de retornar Héroes de DC', () => {

        const owner = 'DC';
        const heroesDC = getHeroesByOwner(owner);

        const heroesDCTest = heroes.filter(heroe => heroe.owner == owner);
        expect(heroesDCTest).toEqual(heroesDC);
    })


    test('debe de retornar Héroes de Marvel', () => {

        const owner = 'Marvel';
        const heroesMarvel = getHeroesByOwner(owner);

        expect(2).toBe(heroesMarvel.length);
    })
})

