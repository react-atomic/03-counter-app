import '@testing-library/jest-dom';
import { getUsuarioActivo, getUser } from "../../base-pruebas/05-funciones";

describe('Prueba en 05-funciones', () => {
    test('getUser debe retornar un Objeto', () => {

        const userTest = {
            uid: 'ABC123',
            username: 'El_Papi1502'
        }

        const user = getUser();

        expect(user).toEqual(userTest);


    });

    test('getUsuarioActivo debe retornar un Objeto', () => {

        const username = 'Pepe';

        const userTest = {
            uid: 'ABC567',
            username
        }

        const userActivo = getUsuarioActivo(username);

        expect(userTest).toEqual(userActivo);

    })


})
