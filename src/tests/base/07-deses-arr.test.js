import { retornaArreglo } from "../../base-pruebas/07-deses-arr"


describe('Pruebas en desestructuracion', () => {

    test('debe retornar un string y un number', () => {

        const [letras, numeros] = retornaArreglo(); // ['ABC',123];


        expect(letras).toBe('ABC')
        expect(typeof letras).toBe('string')

        expect(numeros).toBe(123)
        expect(typeof numeros).toBe('number')
    })

})
