import '@testing-library/jest-dom';
import { getSaludo } from '../../base-pruebas/02-template-string'

describe('Prueba en 02-template-string.js', () => {

    test('getSaludo debe retornar Hola aaron ', () => {
        const nombre = 'aaron';

        const saludo = getSaludo(nombre);

        expect(saludo).toBe('Hola aaron');

    })

    test('getSaludo debe retornar Hola Pepe si no hay argumento en el nombre ', () => {

        const saludo = getSaludo();

        expect(saludo).toBe('Hola Pepe');

    })

})
